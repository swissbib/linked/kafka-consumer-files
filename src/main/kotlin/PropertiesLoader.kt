/*
 * kafka consumer files service
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.swissbib

import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.logging.log4j.Logger
import java.io.File
import java.io.FileInputStream
import java.util.*
import kotlin.system.exitProcess


class PropertiesLoader(
    private val log: Logger
) {
    val appProperties = loadAppProperties()
    val kafkaProperties: Properties = Properties()

    init {
        mapProperties()
    }


    private fun loadAppProperties(): Properties {
        val props = Properties()
        val file = File("/configs/app.properties")
        if (file.isFile)
            props.load(FileInputStream(file))
        else {
            props.load(ClassLoader.getSystemResourceAsStream("app.properties"))
            log.warn("Loading default appProperties from class path! $props")
        }
        log.info("Loaded properties: {}.", props)
        return props
    }

    private fun mapProperties() {
        setProperty(ConsumerConfig.GROUP_ID_CONFIG, abortIfMissing = true)
        setProperty(ConsumerConfig.CLIENT_ID_CONFIG, abortIfMissing = true)
        setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, abortIfMissing = true)
        setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer::class.java)
        setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, SbMetadataDeserializer::class.java)
        setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest")
        setProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true)
        setProperty(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG)
        setProperty(ConsumerConfig.CHECK_CRCS_CONFIG)
        setProperty(ConsumerConfig.CLIENT_DNS_LOOKUP_CONFIG)
        setProperty(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG)
        setProperty(ConsumerConfig.PARTITION_ASSIGNMENT_STRATEGY_CONFIG)
        setProperty(ConsumerConfig.EXCLUDE_INTERNAL_TOPICS_CONFIG)
        setProperty(ConsumerConfig.ISOLATION_LEVEL_CONFIG)
        setProperty(ConsumerConfig.CONNECTIONS_MAX_IDLE_MS_CONFIG)
        setProperty(ConsumerConfig.HEARTBEAT_INTERVAL_MS_CONFIG)
        setProperty(ConsumerConfig.SEND_BUFFER_CONFIG)
        setProperty(ConsumerConfig.INTERCEPTOR_CLASSES_CONFIG)
        setProperty(ConsumerConfig.RETRY_BACKOFF_MS_CONFIG)
        setProperty(ConsumerConfig.REQUEST_TIMEOUT_MS_CONFIG)
        setProperty(ConsumerConfig.METRIC_REPORTER_CLASSES_CONFIG)
        setProperty(ConsumerConfig.METRICS_NUM_SAMPLES_CONFIG)
        setProperty(ConsumerConfig.METRICS_RECORDING_LEVEL_CONFIG)
        setProperty(ConsumerConfig.METRICS_SAMPLE_WINDOW_MS_CONFIG)
        setProperty(ConsumerConfig.METADATA_MAX_AGE_CONFIG)
        setProperty(ConsumerConfig.RECEIVE_BUFFER_CONFIG)
        setProperty(ConsumerConfig.RECONNECT_BACKOFF_MAX_MS_CONFIG)
        setProperty(ConsumerConfig.RECONNECT_BACKOFF_MS_CONFIG)

        setAppProperty("kafka.consumer.topics", abortIfMissing = true)
        setAppProperty("kafka.consumer.poll", "100", abortIfMissing = true)
        setAppProperty("app.maxMessagesPerFile", abortIfMissing = true)
        setAppProperty("app.path", "/data")
        setAppProperty("app.baseFileName", abortIfMissing = true)
        setAppProperty("app.fileExtension", abortIfMissing = true)
        setAppProperty("app.compression", abortIfMissing = true)
        setAppProperty("app.compressionExtension", abortIfMissing = true)
        setAppProperty("app.total", abortIfMissing = true)
    }

    private fun setAppProperty(propertyName: String, defaultValue: Any? = null, abortIfMissing: Boolean = false) {
        val envProperty = propertyName.replace("\\.".toRegex(), "_").toUpperCase()
        when {
            System.getenv(envProperty) != null -> {
                log.debug("Found value for property $propertyName in environment variable $envProperty.")
                appProperties.setProperty(propertyName, System.getenv(envProperty))
            }
            appProperties.getProperty(propertyName) == null && defaultValue != null -> {
                log.debug("No value for $propertyName in appProperties file. Set $defaultValue from default.")
                appProperties[propertyName] = defaultValue
            }
            appProperties.getProperty(propertyName) != null -> {
                log.debug("Use property value from app.properties file: $propertyName=${appProperties.getProperty(propertyName)}")
            }
            abortIfMissing -> {
                log.error("Required property $propertyName not set! Aborting...")
                exitProcess(1)
            }
            else -> log.debug("No value for $propertyName set.")
        }
    }

    private fun setProperty(propertyName: String, defaultValue: Any? = null, abortIfMissing: Boolean = false) {
        val envProperty = propertyName.replace("\\.".toRegex(), "_").toUpperCase()
        when {
            System.getenv(envProperty) != null -> {
                log.trace("Found value for property $propertyName in environment variable $envProperty")
                kafkaProperties.setProperty(propertyName, System.getenv(envProperty))
            }
            appProperties.getProperty(propertyName) != null -> {
                log.trace("Found value for property $propertyName in appProperties file")
                kafkaProperties.setProperty(propertyName, appProperties.getProperty(propertyName))
                appProperties.remove(propertyName)
            }
            defaultValue != null -> {
                kafkaProperties[propertyName] = defaultValue
            }
            abortIfMissing -> {
                log.error("Required property $propertyName not set! Aborting...")
                exitProcess(1)
            }
            else -> log.trace("No value for property $propertyName found")
        }
    }
}
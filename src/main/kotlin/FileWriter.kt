/*
 * kafka consumer files service
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorOutputStream
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.logging.log4j.Logger
import java.io.BufferedWriter
import java.io.FileOutputStream
import java.nio.charset.Charset
import java.util.*

class FileWriter(
    private val consumer: Consumer,
    properties: Properties,
    private val log: Logger
) {

    private val maxMessagesPerFile = properties.getProperty("app.maxMessagesPerFile").toInt()
    private val total = properties.getProperty("app.total").toInt()
    private val path = properties.getProperty("app.path")
    private val baseFileName = properties.getProperty("app.baseFileName")
    private val fileExtension = properties.getProperty("app.fileExtension")
    private val compression = properties.getProperty("app.compression")
    private val compressionExtension = properties.getProperty("app.compressionExtension")


    fun process() {
        var recordCount = 0
        var fileCount = 0
        var writer: BufferedWriter = if (compression == "bzip2") createBZipWriter(fileCount) else createWriter(fileCount)
        while (true) {
            val records = consumer.consume()
            when (compression) {
                "none" -> {
                    for (record: ConsumerRecord<String, SbMetadataModel> in records) {
                        recordCount += 1
                        writeRecord(writer, record.value().data)
                        if (recordCount % maxMessagesPerFile == 0) {
                            writer.flush()
                            writer.close()
                            fileCount += 1
                            writer = createWriter(fileCount)
                        }
                    }
                }
                "bzip2" -> {
                    for (record: ConsumerRecord<String, SbMetadataModel> in records) {
                        recordCount += 1
                        writeRecord(writer, record.value().data)
                        if (recordCount % maxMessagesPerFile == 0) {
                            writer.flush()
                            writer.close()
                            fileCount += 1
                            writer = createBZipWriter(fileCount)
                        }
                    }
                }
            }
            if (recordCount >= total || records.isEmpty) {
                writer.flush()
                writer.close()
                break
            }
        }
    }

    private fun writeRecord(writer: BufferedWriter, message: String) {
        log.info("Writing a message into buffer!")
        writer.write(message)
        writer.newLine()
    }

    private fun createBZipWriter(fileCount: Int): BufferedWriter {
        val id = fileCount.toString().padStart(10, '0')
        val targetPath = "$path/$baseFileName-$id.$fileExtension.$compressionExtension"
        log.info("Write content to file $targetPath.")
        return BZip2CompressorOutputStream(FileOutputStream(targetPath)).bufferedWriter(Charset.defaultCharset())
    }

    private fun createWriter(fileCount: Int): BufferedWriter {
        val id = fileCount.toString().padStart(10, '0')
        val targetPath = "$path/$baseFileName-$id.$fileExtension"
        log.info("Write content to file $targetPath.")
        return FileOutputStream(targetPath).bufferedWriter(Charset.defaultCharset())
    }
}